﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HandelController : MonoBehaviour
{
    public RazorController razorController;
    private Interactable interactable;

    void Start()
    {
        interactable = GetComponent<Interactable>();
    }

    private void onHandHoverBegin(Hand hand)
    {

    }

    private void onHandHoverEnd(Hand hand)
    {
        
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();
        bool isGrabEnding = hand.IsGrabEnding(gameObject);

        if (interactable.attachedToHand == null && grabType != GrabTypes.None)
        {
            hand.AttachObject(gameObject, grabType);
            hand.HoverLock(interactable);
            razorController.can_cut = true;

        }
        else if (isGrabEnding)
        {
            hand.DetachObject(gameObject);
            razorController.can_cut = false;
        }
    }

}
