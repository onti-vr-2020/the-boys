﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class KlinController : MonoBehaviour
{
    public SteamVR_Action_Boolean input;
    public Hand hand1, hand2;

    public bool placed = false;
    public Transform point1;
    public Transform point2;
    public GameObject crashing_object;

    private Interactable interactable;

    void Start()
    {
        interactable = GetComponent<Interactable>();
        hand1 = GameObject.FindGameObjectWithTag("leftHand").GetComponent<Hand>();
        hand2 = GameObject.FindGameObjectWithTag("rightHand").GetComponent<Hand>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Stone" && input.lastState)
        {
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().drag = 126126;
            GetComponent<Rigidbody>().angularDrag = 126126;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            GetComponent<MeshCollider>().convex = false;
            GetComponent<Rigidbody>().isKinematic = true;
            crashing_object = collision.gameObject;
            crashing_object.GetComponent<Rigidbody>().useGravity = false;
            crashing_object.GetComponent<Rigidbody>().drag = 126126;
            crashing_object.GetComponent<Rigidbody>().angularDrag = 126126;
            crashing_object.GetComponent<Rigidbody>().velocity = Vector3.zero;
            crashing_object.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            crashing_object.GetComponent<MeshCollider>().convex = false;
            crashing_object.GetComponent<Rigidbody>().isKinematic = true;
            placed = true;
        }
    }
}
