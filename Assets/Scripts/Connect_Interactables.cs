﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Connect_Interactables : MonoBehaviour
{
    public SteamVR_Action_Boolean input;
    public Hand hand1, hand2;
    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
       
        if(collision.gameObject.tag == "Material")
        {
            
            if(input.lastState)
            {
                
                hand1.DetachObject(this.gameObject);
                hand2.DetachObject(this.gameObject);
                this.GetComponent<Rigidbody>().useGravity = false;
                this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                this.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
                this.GetComponent<Rigidbody>().isKinematic = true;
                this.GetComponent<MeshCollider>().convex = false;
                this.GetComponent<Rigidbody>().drag = 10000;
                this.GetComponent<Rigidbody>().angularDrag = 10000;
            }
            
        }
    }

    private void HandHoverUpdate(Hand hand)
    {
        if(input.state)
        {
            this.GetComponent<Rigidbody>().useGravity = true;
            this.GetComponent<MeshCollider>().convex = true;
            this.GetComponent<Rigidbody>().isKinematic = false;
            this.GetComponent<Rigidbody>().drag = 0;
            this.GetComponent<Rigidbody>().angularDrag = 0;
        }
    }
}
