﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HammerController : MonoBehaviour
{
    public KlinController klinController;
    public CutDown cutDown;

    private Interactable interactable;

    void Start()
    {
        interactable = GetComponent<Interactable>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Klin" && klinController.placed)
        {

            Vector3 start = klinController.point1.position;
            Vector3 end = klinController.point2.position;
            Vector3 depth = klinController.gameObject.transform.position - klinController.point1.position;
            
            var planeTangent = (end - start).normalized;

            if (planeTangent == Vector3.zero)
            {
                planeTangent = Vector3.right;
            }

            var normalVec = Vector3.Cross(depth, planeTangent);

            klinController.crashing_object.GetComponent<Rigidbody>().useGravity = true;
            klinController.crashing_object.GetComponent<Rigidbody>().drag = 1;
            klinController.crashing_object.GetComponent<Rigidbody>().angularDrag = 1;
            klinController.crashing_object.GetComponent<MeshCollider>().convex = true;
            klinController.crashing_object.GetComponent<Rigidbody>().isKinematic = false;

            cutDown.SliceObjects(start, normalVec, new GameObject[]{klinController.crashing_object});
            
            klinController.placed = false;
            collision.gameObject.GetComponent<Rigidbody>().useGravity = true;
            collision.gameObject.GetComponent<Rigidbody>().drag = 1;
            collision.gameObject.GetComponent<Rigidbody>().angularDrag = 1;

        }
    }
}
