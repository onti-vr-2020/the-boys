﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jakkar : MonoBehaviour
{
    public MeshFilter mesh1;
    public MeshFilter mesh2;
    public Mesh mesh_1, mesh_2;

    float SignedVolumeOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float v321 = p3.x * p2.y * p1.z;
        float v231 = p2.x * p3.y * p1.z;
        float v312 = p3.x * p1.y * p2.z;
        float v132 = p1.x * p3.y * p2.z;
        float v213 = p2.x * p1.y * p3.z;
        float v123 = p1.x * p2.y * p3.z;
        return (1.0f / 6.0f) * (-v321 + v231 + v312 - v132 - v213 + v123);
    }

    float VolumeOfMesh(Mesh mesh)
    {
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        float volume = 0f;
        
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Vector3 p1 = vertices[triangles[i]];
            Vector3 p2 = vertices[triangles[i + 1]];
            Vector3 p3 = vertices[triangles[i + 2]];
            volume += SignedVolumeOfTriangle(p1, p2, p3);
        }
        return Mathf.Abs(volume);
    }
    
    void Start()
    {
        mesh_1 = mesh1.mesh;
        mesh_2 = mesh2.mesh;
    }

    void Update()
    {
        
    }

    // void Jaccard(Mesh mesh1, Mesh mesh2)
    // {
    //     float volumeA, volumeB, volumeC;
    //     volumeA = VolumeOfMesh(mesh1);
    //     volumeB = VolumeOfMesh(mesh2);

    //     BoxCollider col1, col2;
    //     col1 = mesh1.gameObject.GetComponent<BoxCollider>();
    //     col2 = mesh2.gameObject.GetComponent<BoxCollider>();


    // }
}
