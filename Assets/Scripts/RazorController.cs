﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RazorController : MonoBehaviour
{

    public CutDown cutDown;
    public bool can_cut = false;


    public Transform point1;
    public Transform point2;
    public Transform point3;

    private BoxCollider collider;
    private Vector3 start_cutting_pos;
    private Vector3 start_cutting_rot;
    private GameObject cutting_object;

    void Start()
    {
        collider = GetComponent<BoxCollider>();
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Wood")
        {
            start_cutting_pos = transform.position;
            start_cutting_rot = transform.localRotation.eulerAngles;
            cutting_object = other.gameObject;
        }
        
        Debug.Log("Enter");

    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log("Stay");

        if (cutting_object == null)
            return;
        
        if (transform.position.y < cutting_object.transform.position.y - cutting_object.GetComponent<MeshCollider>().bounds.size.y / 3.2f  && cutting_object.tag == "Wood")
        {
            Vector3 start = point1.position;
            Vector3 end = point2.position;
            Vector3 depth = transform.position - point1.position;
            
            // cutDown.DrawPlane(start, end, depth);

            var planeTangent = (end - start).normalized;

            if (planeTangent == Vector3.zero)
            {
                planeTangent = Vector3.right;
            }

            var normalVec = Vector3.Cross(depth, planeTangent);

            cutDown.SliceObjects(start, normalVec, new GameObject[]{cutting_object});

            cutting_object = null;
        }
        
        
    }

    void Update()
    {
    }

}
