﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors_2: MonoBehaviour
{
    public Transform player;
    public GameObject t1;
    public GameObject t2;
    public GameObject t3;
    public bool opened=false;

    void Start()
    {
    player = GameObject.Find("Player").transform;
    }

    void Update()
    {
        float z=transform.position.z;
        float x=transform.position.x;
        float res=Mathf.Sqrt(Mathf.Pow((x-player.position.x),2)+Mathf.Pow((z-player.position.z),2));
        if( res>7f && opened)
        {  
            this.GetComponent<Animation>().Play("close");
            opened=false;
            
        }
        else if( res<7f && !opened)
        {
            this.GetComponent<Animation>().Play("open");
            opened=true;
            t1.SetActive(true);
            t2.SetActive(true);
            t3.SetActive(true);
        }



}
}
