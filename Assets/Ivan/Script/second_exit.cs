﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class second_exit : MonoBehaviour
{   
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        float z1=transform.position.z;
        float x1=transform.position.x;
        float res1=Mathf.Sqrt(Mathf.Pow((x1-player.position.x),2)+Mathf.Pow((z1-player.position.z),2));
        if( res1<1f)
        {  
            Application.Quit();
        }
    }
}
