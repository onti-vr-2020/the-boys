﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class level_teleport_3 : MonoBehaviour
{
   public Transform player;


    void Start()
    {
        
    }

    void Update()
    {
        float z1=transform.position.z;
        float x1=transform.position.x;
        float res1=Mathf.Sqrt(Mathf.Pow((x1-player.position.x),2)+Mathf.Pow((z1-player.position.z),2));
        if( res1<3f)
        {  
          SceneManager.LoadScene("Level 3");
            
        }}
}