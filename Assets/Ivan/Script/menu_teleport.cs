﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_teleport : MonoBehaviour
{
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        float z=player.position.z;
        float x=player.position.x;
        float res1=Mathf.Sqrt(Mathf.Pow((x-transform.position.x),2)+Mathf.Pow((z-transform.position.z),2));
        Debug.Log(res1);
        if(res1<2f)
        {  
            SceneManager.LoadScene("Second_menu");
        }
        
    }
}
